package com.lean.tecnical_test.controller;

import com.lean.tecnical_test.model.Employee;
import com.lean.tecnical_test.model.Position;
import com.lean.tecnical_test.repository.EmployeeRepository;
import com.lean.tecnical_test.repository.PositionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@CrossOrigin(origins = "http://*")
@RestController
@RequestMapping("/api/position")
public class PositionController {

    @Autowired
    PositionRepository positionRepository;

    @Autowired
    EmployeeRepository employeeRepository;



    @PostMapping(value = "/create")
    public ResponseEntity<Position> createPositions ()
    {
        List<Position> positionList = new ArrayList<>();
        Position p1 = new Position(1,"dev");
        Position p2 = new Position(2,"qa");
        positionList.add(p1);
        positionList.add(p2);
        return new ResponseEntity(positionRepository.saveAll(positionList), HttpStatus.CREATED);
    }

    @GetMapping("/list")
    public ResponseEntity<Object> list()
    {
        List<Position> positions = new ArrayList<>();
        List<Employee> employees;
        positionRepository.findAll().forEach(positions::add);
        Map<String,Object> map;
        List<Object> map2 = new ArrayList<>();
        for(Integer i = 0 ; i < positions.size() ; i++)
        {
            map = new HashMap<String,Object>();
            employees = new ArrayList<>();
            Long id = positions.get(i).getId().longValue() ;
            employeeRepository.findByIdPositionOS(id).forEach(employees::add);
            map.put( "id", positions.get(i).getId());
            map.put("name",positions.get(i).getName());
            map.put("employees",employees);
            map2.add(map);
        }
        return new ResponseEntity<>(map2, HttpStatus.OK);
    }

}
