package com.lean.tecnical_test.controller;

import com.lean.tecnical_test.model.Employee;
import com.lean.tecnical_test.model.Person;
import com.lean.tecnical_test.repository.EmployeeRepository;
import com.lean.tecnical_test.repository.PersonRepository;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "http://*")
@RestController
@RequestMapping("/api")
public class EmployeeController {

    @Autowired
    EmployeeRepository employeeRepository;

    @Autowired
    PersonRepository personRepository;

    @GetMapping(value = "/employee/salary")
    public ResponseEntity<List<Employee>> getAllEmployees( @RequestParam(required = false) String salary )
    {
        try{
            List<Employee> employees = new ArrayList<Employee>();

            if (  (salary == null || salary == "") )
            {
                employeeRepository.findAll().forEach(employees::add);
            }

            if ( !(salary == null) && !(salary == "") )
            {
                employeeRepository.findBySalary(Double.parseDouble(salary)).forEach(employees::add);
            }
            if (employees.isEmpty()){
                String msg =
                        "[" +
                                "{" +
                                "\"info\":\"No Data to retrieve\"" +
                                "}" +
                                "]";
                HttpHeaders responseHeaders = new HttpHeaders();
                responseHeaders.setContentType(MediaType.APPLICATION_JSON);
                return new ResponseEntity(msg, responseHeaders, HttpStatus.OK);
            }else {
                return new ResponseEntity<>(employees, HttpStatus.OK);
            }
        } catch(Exception e){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(value = "/employee")
    public ResponseEntity<List<Employee>> getAllEmployees( @RequestParam(required = false) String name, @RequestParam(required = false) String position )
    {
        try{
            List<Employee> employees = new ArrayList<Employee>();
            List<Object>  emp = new ArrayList<Object>();
            if ( (name == null || name == "" )  && (position == null || position == "") )
            {
                employeeRepository.findAll().forEach(employees::add);
            }

            if ( !(name == null) && !(name == "") )
            {

                employeeRepository.findByName(name).forEach(employees::add);
            }

            if ( !(position == null) && !(position == "") )
            {
                Long localPosition = Long.parseLong(position);
                employeeRepository.findByIdPosition(localPosition).forEach(employees::add);
            }

            if (employees.isEmpty()){
                String msg =
                        "[" +
                            "{" +
                                "\"info\":\"No Data to retrieve\"" +
                            "}" +
                        "]";
                HttpHeaders responseHeaders = new HttpHeaders();
                responseHeaders.setContentType(MediaType.APPLICATION_JSON);
                return new ResponseEntity(msg, responseHeaders, HttpStatus.OK);
            }else {
                return new ResponseEntity<>(employees, HttpStatus.OK);
            }
        } catch(Exception e){
             return new ResponseEntity(" " + e, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/employee")
    public ResponseEntity<Employee> createEmployee (@RequestBody String data)
    {
        try{
            Employee localEmployee;
            JSONObject json = new JSONObject(data);
            Long temp = personRepository.getLastId();
            Long personId = 1L;
            if ( !(temp == null)) personId = temp + 1;
            localEmployee = new Employee(
                    personId,
                    json.getLong("idPosition"),
                    json.getDouble("salary"),
                    json.getString("name"),
                    json.getString("lastName"),
                    json.getString("address"),
                    json.getString("cellphone"),
                    json.getString("cityName")
            );
            return new ResponseEntity<>(employeeRepository.save(localEmployee), HttpStatus.CREATED);
        }catch (Exception e){
            return new ResponseEntity("Exeption: "+e, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/employee")
    public ResponseEntity<Employee> updateEmployee (@RequestParam(required = false) Long idEmployee, @RequestBody String data)
    {
        try{
                Optional<Employee> employeeInfo  = employeeRepository.findById(idEmployee);
                Optional<Person> personInfo = personRepository.findById(idEmployee);
                JSONObject json = new JSONObject(data);
                if (personInfo.isPresent() && employeeInfo.isPresent())
                {
                    Person tempPerson = personInfo.get();
                    tempPerson.setName(json.getString("name"));
                    tempPerson.setLastName(json.getString("lastName"));
                    tempPerson.setAddress(json.getString("address"));
                    tempPerson.setCellphone(json.getString("cellphone"));
                    tempPerson.setCityName(json.getString("cityName"));
                    personRepository.save(tempPerson);

                    Employee tempEmployee = employeeInfo.get();
                    tempEmployee.setIdPosition(json.getLong("idPosition"));
                    tempEmployee.setSalary(json.getDouble("salary"));
                    employeeRepository.save(tempEmployee);

                    String msg =
                            "[" +
                                    "{" +
                                    "\"info\":\"Data updated successfully\"" +
                                    "}" +
                                    "]";
                    HttpHeaders responseHeaders = new HttpHeaders();
                    responseHeaders.setContentType(MediaType.APPLICATION_JSON);
                    return new ResponseEntity(msg, responseHeaders, HttpStatus.OK);
                }else {
                    String msg =
                            "[" +
                                    "{" +
                                    "\"info\":\"Not content found\"" +
                                    "}" +
                                    "]";
                    HttpHeaders responseHeaders = new HttpHeaders();
                    responseHeaders.setContentType(MediaType.APPLICATION_JSON);
                    return new ResponseEntity(msg, responseHeaders, HttpStatus.NO_CONTENT);
                }
            }catch (Exception e){
            return new ResponseEntity(" "+e, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @DeleteMapping("/employee")
    public ResponseEntity<HttpStatus> deleteEmployee(@RequestParam(required = false) Long idEmployee)
    {
        try {
            employeeRepository.deleteById(idEmployee);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
