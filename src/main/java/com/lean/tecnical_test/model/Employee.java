package com.lean.tecnical_test.model;

import javax.persistence.*;

@Entity
@Table(name = "employee")
@SecondaryTable( name = "person", pkJoinColumns = @PrimaryKeyJoinColumn(name = "idPerson") )
public class Employee{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idEmployee;

    @Column(name = "idPerson")
    private Long idPerson;

    @Column(name = "idPosition")
    private Long idPosition;

    @Column(name = "salary")
    private Double salary;

    /*-------------------------------*/

    @Column(name = "name", table="person")
    private String name;

    @Column(name = "lastName", table="person")
    private String lastName;

    @Column(name = "address", table="person")
    private String address;

    @Column(name = "cellphone", table="person")
    private String cellphone;

    @Column(name = "cityName", table="person")
    private String cityName;

    public Employee(){}

    public Employee(Long idPerson, Long idPosition, Double salary, String name, String lastName, String address, String cellphone, String cityName) {
        this.idPerson = idPerson;
        this.idPosition = idPosition;
        this.salary = salary;
        this.name = name;
        this.lastName = lastName;
        this.address = address;
        this.cellphone = cellphone;
        this.cityName = cityName;
    }

    public Long getIdEmployee() {
        return idEmployee;
    }

    public Long getIdPerson() {
        return idPerson;
    }

    public void setIdPerson(Long idPerson) {
        this.idPerson = idPerson;
    }

    public Long getIdPosition() {
        return idPosition;
    }

    public void setIdPosition(Long idPosition) {
        this.idPosition = idPosition;
    }

    public Double getSalary() {
        return salary;
    }

    public void setSalary(Double salary) {
        this.salary = salary;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCellphone() {
        return cellphone;
    }

    public void setCellphone(String cellphone) {
        this.cellphone = cellphone;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + idEmployee +
                ", idPerson=" + idPerson +
                ", position='" + idPosition + '\'' +
                ", salary=" + salary +
                '}';
    }
}
