package com.lean.tecnical_test.repository;

import com.lean.tecnical_test.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface EmployeeRepository extends JpaRepository<Employee, Long> {

    List<Employee> findByIdPerson( Long idPerson);

    List<Employee> findByIdPosition(Long idPosition);

    @Query(nativeQuery = true, value = "SELECT employee.*, person.* FROM employee JOIN person ON employee.ID_PERSON = person.ID_PERSON WHERE ID_POSITION = ?1 ORDER BY employee.SALARY DESC")
    List<Employee> findByIdPositionOS(Long idPosition);

    List<Employee> findBySalary (double salary);

    @Query( value = " SELECT employee.*, person.* FROM employee  JOIN person on employee.ID_PERSON = person.ID_PERSON  WHERE person.NAME LIKE %?1% ", nativeQuery = true)
    List<Employee> findByName (String name);


}
