package com.lean.tecnical_test.repository;

import com.lean.tecnical_test.model.Employee;
import com.lean.tecnical_test.model.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface PersonRepository extends JpaRepository<Person, Long> {

   List<Person> findByName (String name);
   List<Person> findByLastName (String lastName);
   List<Person> findByAddress (String address);

   @Query(value = "SELECT person.ID_PERSON FROM person ORDER BY ID_PERSON DESC LIMIT 1", nativeQuery = true)
   Long getLastId();

}
