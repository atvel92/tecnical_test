package com.lean.tecnical_test.repository;

import com.lean.tecnical_test.model.Person;
import com.lean.tecnical_test.model.Position;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PositionRepository extends JpaRepository<Position, Long> {
}
